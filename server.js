const http = require('http');
const fs = require('fs');
const { WebSocketServer } = require('ws');

const server = http.createServer((req, res) => {
  const content = fs.readFileSync('index.html');
  res.end(content.toString());
});

const connections = [];
const wss = new WebSocketServer({ server });

wss.on('connection', function (socket) {
  console.log('Client connected!');
  connections.push(socket);

  socket.on('message', (data) => {
    console.log('Message received ' + data);
    connections.forEach((connection) => {
      connection.send(data.toString());
    });
  });
});

server.listen(8080, () => {
  console.log('Server is running on port 8080');
});
